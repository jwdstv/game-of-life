﻿using System.Runtime.InteropServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GameOfLife;

namespace GameOfLifeTests
{
    [TestClass]
    public class TestGameOfLife
    {
        private readonly GameOfLifeEngine game = new GameOfLifeEngine(new CellStateCalculator(), new HashBoardProvider());

        [TestMethod]
        public void TestPatternOne()
        {
            var initBoard = CreateInitBoard1();
            var boardIterationOne = game.NextGeneration(initBoard);
            Assert.AreEqual(boardIterationOne.LiveCellsCount(), PatternOneBoardIterationOne().LiveCellsCount());
            Assert.AreEqual(boardIterationOne, PatternOneBoardIterationOne());
        }

        [TestMethod]
        public void TestPatternTwo()
        {
            var initBoard = CreateInitBoard2();
            var boardIterationOne = game.NextGeneration(initBoard);
            Assert.AreEqual(boardIterationOne.LiveCellsCount(), PatternTwoBoardIterationOne().LiveCellsCount());
            Assert.AreEqual(boardIterationOne, PatternTwoBoardIterationOne());
        }

        private static IBoard PatternTwoBoardIterationOne()
        {
            var initBoard = new HashBoard();
            initBoard.AddCell(new Cell(3, 1, true));
            initBoard.AddCell(new Cell(3, 2, true));
            initBoard.AddCell(new Cell(4, 1, true));
            initBoard.AddCell(new Cell(4, 2, true));
            return initBoard;
        }

        private static IBoard CreateInitBoard2()
        {
            var initBoard = new HashBoard();
            initBoard.AddCell(new Cell(1, 2, true));
            initBoard.AddCell(new Cell(3, 2, true));
            initBoard.AddCell(new Cell(4, 1, true));
            initBoard.AddCell(new Cell(4, 2, true));
            return initBoard;
        }

        [TestMethod]
        public void TestPatternThree()
        {
            var initBoard = CreateInitBoard3();
            var boardIterationOne = game.NextGeneration(initBoard);

            Assert.AreEqual(boardIterationOne, PatternThreeBoardIterationOne());

            var boardIterationTwo = game.NextGeneration(boardIterationOne);
            Assert.AreEqual(boardIterationTwo, PatternThreeBoardIterationTwo());

            var boardIterationThree = game.NextGeneration(boardIterationTwo);
            Assert.AreEqual(boardIterationThree, PatternThreeBoardIterationThree());
        }

        private static IBoard PatternThreeBoardIterationTwo()
        {
            var initBoard = new HashBoard();
            initBoard.AddCell(new Cell(0, 0, true));
            initBoard.AddCell(new Cell(0, 1, true));
            initBoard.AddCell(new Cell(0, 2, true));

            initBoard.AddCell(new Cell(2, 2, true));
            initBoard.AddCell(new Cell(3, 2, true));
            initBoard.AddCell(new Cell(4, 2, true));
            initBoard.AddCell(new Cell(3, 3, true));
            initBoard.AddCell(new Cell(4, 3, true));
            return initBoard;
        }

        private static IBoard PatternThreeBoardIterationThree()
        {
            var initBoard = new HashBoard();
            initBoard.AddCell(new Cell(-1, 1, true));
            initBoard.AddCell(new Cell(0, 1, true));
            initBoard.AddCell(new Cell(3, 1, true));
            initBoard.AddCell(new Cell(1, 2, true));

            initBoard.AddCell(new Cell(2, 2, true));
            initBoard.AddCell(new Cell(4, 2, true));
            initBoard.AddCell(new Cell(2, 3, true));
            initBoard.AddCell(new Cell(4, 3, true));
            return initBoard;
        }

        private static IBoard CreateInitBoard3()
        {
            var initBoard = new HashBoard();
            initBoard.AddCell(new Cell(0, 0, true));
            initBoard.AddCell(new Cell(0, 1, true));
            initBoard.AddCell(new Cell(0, 2, true));
            initBoard.AddCell(new Cell(3, 2, true));
            initBoard.AddCell(new Cell(4, 2, true));
            initBoard.AddCell(new Cell(3, 3, true));
            initBoard.AddCell(new Cell(5, 3, true));
            return initBoard;
        }

        private static IBoard PatternThreeBoardIterationOne()
        {
            var initBoard = new HashBoard();
            initBoard.AddCell(new Cell(-1, 1, true));
            initBoard.AddCell(new Cell(0, 1, true));
            initBoard.AddCell(new Cell(1, 1, true));
            initBoard.AddCell(new Cell(3, 2, true));
            initBoard.AddCell(new Cell(4, 2, true));
            initBoard.AddCell(new Cell(3, 3, true));
            return initBoard;
        }

        private static IBoard CreateInitBoard1()
        {
            var initBoard = new HashBoard();
            initBoard.AddCell(new Cell(2, 2, true));
            initBoard.AddCell(new Cell(1, 3, true));
            initBoard.AddCell(new Cell(2, 3, true));
            initBoard.AddCell(new Cell(3, 3, true));
            return initBoard;
        }

        private static IBoard PatternOneBoardIterationOne()
        {
            var initBoard = new HashBoard();
            initBoard.AddCell(new Cell(2, 2, true));
            initBoard.AddCell(new Cell(1, 3, true));
            initBoard.AddCell(new Cell(2, 3, true));
            initBoard.AddCell(new Cell(3, 3, true));
            initBoard.AddCell(new Cell(1, 2, true));
            initBoard.AddCell(new Cell(3, 2, true));
            initBoard.AddCell(new Cell(2, 4, true));
            return initBoard;
        }
    }
}
