﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameOfLife
{
    public class HashBoard : IBoard
    {
        private Dictionary<Coordinate, Cell> _board;

        public HashBoard()
        {
            _board = new Dictionary<Coordinate, Cell>();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            var board2 = obj as HashBoard;
            if (board2 == null)
                return false;

            foreach (var cell in _board)
            {
                if (!board2.GetCell(cell.Key).Equals(_board[cell.Key]))
                    return false;
            }
            return true;
        }

        public List<Cell> GetNeighbours(Cell cell)
        {
            var neighborCoordinates = Coordinate.GetNeighbourCoordinates(cell.Coordinate);
            var neighbours = new List<Cell>();
            foreach (var neighborCoordinate in neighborCoordinates)
            {
                var neighbourCell = GetCell(neighborCoordinate);
                neighbours.Add(neighbourCell);
            }

            return neighbours;
        }

        public List<Cell> GetLiveNeighbours(Cell cell)
        {
            throw new NotImplementedException();
        }

        public Cell GetCell(Coordinate coordinate)
        {
            //var cell = _board[coordinate];
            Cell cell;
            _board.TryGetValue(coordinate, out cell);
            if (cell == null)
            {
                cell = new Cell(coordinate.XPos, coordinate.YPos);
            }

            return cell;
        }


        public void AddCell(Cell cell)
        {
            if (cell.IsAlive)
            {
                _board.Add(cell.Coordinate, cell);
            }
        }


        public IList<Cell> GetAllApplicableCells()
        {
            var allApplicableCells = new Dictionary<Coordinate, Cell>();
            foreach (var key in _board.Keys)
            {
                allApplicableCells.Add(key, _board[key]);
            }
            var liveCells = _board;
            foreach (var liveCell in liveCells.Values)
            {
                var liveCellNeighbourCells = GetNeighbours(liveCell);
                foreach (var cell in liveCellNeighbourCells)
                {
                    if (!allApplicableCells.ContainsKey(cell.Coordinate))
                    {
                        allApplicableCells.Add(cell.Coordinate, cell);
                    }
                }
            }

            return allApplicableCells.Values.ToList();
        }


        public int LiveCellsCount()
        {
            return _board.Count;
        }
    }
}
