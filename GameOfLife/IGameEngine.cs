﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    public interface IGameEngine
    {
        IBoard NextGeneration(IBoard previousBoard);
    }
}
