﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    public class GameOfLifeEngine : IGameEngine
    {
        private readonly ICellStateCalculator _cellCalculator;
        private readonly IBoardProvider _boardProvider;

        public GameOfLifeEngine(ICellStateCalculator cellCalculator, IBoardProvider boardProvider)
        {
            _cellCalculator = cellCalculator;
            _boardProvider = boardProvider;
        }

        public IBoard NextGeneration(IBoard previousBoard)
        {
            var cellsToEvaluate = previousBoard.GetAllApplicableCells();
            var newBoard = _boardProvider.NewBoard();

            cellsToEvaluate.Select(cell => _cellCalculator.CalculateNextState(cell, previousBoard.GetNeighbours(cell)) ).ToList()
                .ForEach(newBoard.AddCell);

            //foreach (var newCell in cellsToEvaluate.Select(cell => _cellCalculator.CalculateNextState(cell, previousBoard.GetNeighbours(cell))))
            //{
            //    newBoard.AddCell(newCell);
            //}

            return newBoard;
        }
    }
}
