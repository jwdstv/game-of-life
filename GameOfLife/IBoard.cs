﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameOfLife
{
    public interface IBoard
    {
        List<Cell> GetNeighbours(Cell cell);

        List<Cell> GetLiveNeighbours(Cell cell);

        Cell GetCell(Coordinate coordinate);

        void AddCell(Cell cell);

        IList<Cell> GetAllApplicableCells();

        int LiveCellsCount();

        bool Equals(object obj);
    }
}
