﻿namespace GameOfLife
{
    public class HashBoardProvider : IBoardProvider
    {
        public IBoard NewBoard()
        {
            return new HashBoard();
        }
    }
}