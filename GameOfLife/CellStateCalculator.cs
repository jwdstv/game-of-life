﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    public class CellStateCalculator : ICellStateCalculator
    {
        public Cell CalculateNextState(Cell currentCell, IList<Cell> neighborCells)
        {
            bool nextCellState = false;
            var liveNeighbors = neighborCells.Count(x => x.IsAlive);
            if (currentCell.IsAlive)
            {
                //if (liveNeighbors < 2)
                //    nextCellState = false;
                if (liveNeighbors == 3 || liveNeighbors == 2)
                {
                    nextCellState = true;
                }
                //if (liveNeighbors > 3)
                //     nextCellState =  false;
            }
            else if (liveNeighbors == 3)
            {
                nextCellState = true;
            }

            return nextCellState != currentCell.IsAlive ? new Cell(currentCell.Coordinate.XPos, currentCell.Coordinate.YPos, nextCellState) : currentCell;
            
        }
    }
}
