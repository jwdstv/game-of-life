﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    public class Cell
    {
        public Cell(int xpos, int ypos)
        {
            if (Coordinate == null)
            {
                Coordinate = new Coordinate(xpos, ypos);
            }
        }

        //TODO: duplicate constructor?
        public Cell(int xpos, int ypos, bool state)
        {
            if (Coordinate == null)
            {
                Coordinate = new Coordinate(xpos, ypos);
            }
            IsAlive = state;
        }

        protected bool Equals(Cell other)
        {
            return Equals(Coordinate, other.Coordinate) && IsAlive.Equals(other.IsAlive);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Coordinate != null ? Coordinate.GetHashCode() : 0)*397) ^ IsAlive.GetHashCode();
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Cell) obj);
        }


        public Coordinate Coordinate { get; set; }
        public bool IsAlive { get; set; }
    }
}
