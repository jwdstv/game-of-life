﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    public class Coordinate
    {
        public Coordinate(int xpos, int ypos)
        {
            XPos = xpos;
            YPos = ypos;
        }

        protected bool Equals(Coordinate other)
        {
            return XPos == other.XPos && YPos == other.YPos;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (XPos*397) ^ YPos;
            }
        }
        
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Coordinate) obj);
        }

        public int XPos { get; set; }
        public int YPos { get; set; }

        internal static IList<Coordinate> GetNeighbourCoordinates(Coordinate centerCoordinate)
        {
            var coordinates = new List<Coordinate>
            {
                new Coordinate(centerCoordinate.XPos - 1, centerCoordinate.YPos-1),
                new Coordinate(centerCoordinate.XPos    , centerCoordinate.YPos-1),
                new Coordinate(centerCoordinate.XPos + 1, centerCoordinate.YPos-1),
                new Coordinate(centerCoordinate.XPos - 1, centerCoordinate.YPos),
                new Coordinate(centerCoordinate.XPos + 1, centerCoordinate.YPos),
                new Coordinate(centerCoordinate.XPos - 1, centerCoordinate.YPos+1),
                new Coordinate(centerCoordinate.XPos    , centerCoordinate.YPos+1),
                new Coordinate(centerCoordinate.XPos + 1, centerCoordinate.YPos+1),
            };
            
            return coordinates;
        }
    }
}
